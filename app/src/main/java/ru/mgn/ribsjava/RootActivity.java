package ru.mgn.ribsjava;

import android.view.ViewGroup;

import com.uber.rib.core.RibActivity;
import com.uber.rib.core.ViewRouter;

import ru.mgn.ribsjava.root.RootBuilder;

public class RootActivity extends RibActivity {
    @Override
    protected ViewRouter<?, ?, ?> createRouter(ViewGroup parentViewGroup) {
        RootBuilder rootBuilder = new RootBuilder(new RootBuilder.ParentComponent() {
        });

        return rootBuilder.build(parentViewGroup);
    }
}
